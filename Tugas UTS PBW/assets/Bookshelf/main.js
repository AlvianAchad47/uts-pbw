const book = []
const RENDER_BOOK = "render-book"
const STORAGE_KEY = "data-book"

document.addEventListener("DOMContentLoaded", function(){
    const submitForm = document.getElementById("inputBook")
    submitForm.addEventListener("submit", function(event){
        event.preventDefault()
        addBook()
    })

    const searchForm = document.getElementById("searchBook")
    searchForm.addEventListener("submit", function(event){
        event.preventDefault()
        searchBook()
    })

    if (isStorageExist()){
        loadBookFromStorage()
    }
})

function searchBook(){
    const titleBook = document.getElementById("searchBookTitle").value
    const objTitleBook = findTitle(titleBook)

    if (!objTitleBook){
        alert("Buku kamu tidak ditemukan")
    } else {
        renderTitle(objTitleBook)
    }
}

function findTitle(title){
    for (const obj of book){
        if (obj.title === title){
            return obj
        } 
    }
    return false
}

function renderTitle(objTitle){
    const search = document.getElementById("search")
    search.innerHTML = ''
    
    const container = makeTitle(objTitle)
    search.append(container)
}

function makeTitle(objTitle){
    const container = document.createElement("div")
    container.classList.add("book_list_search")

    const judul = document.createElement("h3")
    judul.innerText = objTitle.title

    const penulis = document.createElement("p")
    penulis.innerText = `Penulis : ${objTitle.author}`

    const tahun = document.createElement("p")
    tahun.innerText = `Tahun : ${objTitle.year}`

    const closeButton = document.createElement("button")
    closeButton.innerText = 'close'
    closeButton.classList.add("red")

    closeButton.addEventListener("click", function(){
        const search = document.getElementById("search")
        search.innerHTML = ''
    })

    container.append(judul, penulis, tahun, closeButton)
    return container
}

function addBook(){
    const bookTitle = document.getElementById("inputBookTitle").value
    const bookAuthor = document.getElementById("inputBookAuthor").value
    const bookYear = document.getElementById("inputBookYear").value
    const isComplete = document.getElementById("inputBookIsComplete").checked
    const generateID = generateId()

    const bookObject = generateBookObject(generateID, bookTitle, bookAuthor, Number(bookYear), isComplete )
    book.push(bookObject)

    document.dispatchEvent(new Event(RENDER_BOOK))
    sendBook()
}

function generateId(){
    return +new Date()
}
function generateBookObject(id, title, author, year, isComplete){
    return {
        id,
        title,
        author,
        year,
        isComplete
    }
}

document.addEventListener(RENDER_BOOK, function(){
    const uncompletedBook = document.getElementById("incompleteBookshelfList")
    uncompletedBook.innerHTML = ''

    const completedBook = document.getElementById("completeBookshelfList")
    completedBook.innerHTML = ''

    for (const bookItem of book){
        const bookElement = makeBook(bookItem)
        if (bookItem.isComplete){
            completedBook.append(bookElement)
        } else {
            uncompletedBook.append(bookElement)
        }
    }
})

function makeBook(objekBuku){
    const bookContainer = document.createElement("article")
    bookContainer.classList.add("book_item")

    const bookTitle = document.createElement("h3")
    bookTitle.innerText = objekBuku.title

    const bookAuthor = document.createElement("p")
    bookAuthor.innerText = `Penulis: ${objekBuku.author}`

    const bookYear = document.createElement("p")
    bookYear.innerText = `Tahun: ${objekBuku.year}`

    bookContainer.append(bookTitle, bookAuthor, bookYear)

    const buttonContainer = document.createElement("div")
    buttonContainer.classList.add("action")

    if (objekBuku.isComplete){
        const unfinishedButton = document.createElement("button")
        unfinishedButton.classList.add("green")
        unfinishedButton.innerText = "Belum Selesai"

        unfinishedButton.addEventListener("click", function(){
            unfinishedBook(objekBuku.id)
        })

        const deleteButton = document.createElement("button")
        deleteButton.classList.add("red")
        deleteButton.innerText = "Hapus Buku"

        deleteButton.addEventListener("click", function(){
            deleteBook(objekBuku.id) 
            popUpFinished()
        })

        buttonContainer.append(unfinishedButton, deleteButton)
        bookContainer.append(buttonContainer)
    } else {
        const finishedButton = document.createElement("button")
        finishedButton.classList.add("green")
        finishedButton.innerText = "Sudah Selesai"

        finishedButton.addEventListener("click", function(){
            finishedBook(objekBuku.id) 
        })

        const deleteButton = document.createElement("button")
        deleteButton.classList.add("red")
        deleteButton.innerText = "Hapus Buku"

        deleteButton.addEventListener("click", function(){
            deleteBook(objekBuku.id) 
            popUpUnfinished()
        })

        buttonContainer.append(finishedButton, deleteButton)
        bookContainer.append(buttonContainer)
    }

    return bookContainer
}

function finishedBook(bookId){
    const bookTarget = findBook(bookId)

    if (bookTarget == null) return 

    bookTarget.isComplete = true
    document.dispatchEvent(new Event(RENDER_BOOK))
    sendBook()
}

function unfinishedBook(bookId){
    const bookTarget = findBook(bookId)

    if (bookTarget == null) return

    bookTarget.isComplete = false
    document.dispatchEvent(new Event(RENDER_BOOK))
    sendBook()
}

function findBook(bookId){
    for (const bookElm of book){
        if (bookElm.id === bookId){
            return bookElm
        }
    }
    return null
}

function deleteBook(bookId){
    const bookIndex = findIndex(bookId)

    book.splice(bookIndex, 1)
    document.dispatchEvent(new Event(RENDER_BOOK))
    sendBook()
}

function popUpFinished(){
    const popup = document.getElementById("finished")
    popup.removeAttribute("hidden")

    const clsButton = document.getElementById("finishedButton")
    clsButton.addEventListener("click", function(){
        const clsPopUp = document.getElementById("finished")
        clsPopUp.hidden = true
    })
}

function popUpUnfinished(){
    const popup = document.getElementById("unfinished")
    popup.removeAttribute("hidden")

    const clsButton = document.getElementById("unfinishedButton")
    clsButton.addEventListener("click", function(){
        const clsPopUp = document.getElementById("unfinished")
        clsPopUp.hidden = true
    })
}

function findIndex(bookId){
    for (const index in book){
        if (book[index].id === bookId){
            return index
        }
    }
    return -1
}

function sendBook(){
    if (isStorageExist()){
        const data = JSON.stringify(book)
        localStorage.setItem(STORAGE_KEY, data)

    }
}

function isStorageExist(){
    if (typeof Storage === "undefined"){
        alert("Browser Kamu tidak mendukung")
        return false
    }
    return true
}

function loadBookFromStorage(){
    const data = localStorage.getItem(STORAGE_KEY)
    const bookData = JSON.parse(data)
    if (bookData !== null){
        for (const bookObject of bookData){
            book.push(bookObject)
        }
    }

    document.dispatchEvent(new Event(RENDER_BOOK))
}
